/*
	Mini-Activity:
		Create an express JS designated to port 4000
		Create a new route with endpoint /hello and method GET
			-should be able to respond with "Hello World"




*/
//express server
const express = require("express");

//Mongoose is a package that allows creation of Schemas to model our data structures
//Also has access to a number of methods for manipulating our databse

const mongoose = require("mongoose");

const app = express();

app.use(express.json());

const port= 4000;

//MongoDB Connection

/*
	Syntax:
	mongoose.connect("<MongoDB Atlas Connection String>")

	Sample connection string:
	"mongodb+srv://admin:admin123@batch204-ericjohnmagang.yrt1nur.mongodb.net/B204-to-dos?retryWrites=true&w=majority"

*/
mongoose.connect("mongodb+srv://admin:admin123@batch204-ericjohnmagang.yrt1nur.mongodb.net/B204-to-dos?retryWrites=true&w=majority",
{
	useNewUrlParser:true,
	useUnifiedTopology:true
}
);


//Set notifications for connection success or failuer
let db = mongoose.connection;

//If a connection error occured, output in the console
db.on("error",console.error.bind(console, "Connection Error"));
//if the connection is successful, output in the console
db.once("open", () => console.log("Were connected to the cloud database."));

//---------------------------------------------------

//Mongoose Schema
//Schemas determine the structure of the documents to be writtin in the database
//Schemas acts as blueprints to our data
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type:String,
		default: "pending"
	}
});

//Models
//The first parameter of the Mongoose model method indicates the collection in where to store the data
//The second parameter is used to specify the schema/blueprint of the documents thar will be stored in the MongoDB collection

const Task = mongoose.model("Task", taskSchema);

//------------------------------------
app.use(express.json());



app.get("/hello", (req, res) =>{
	
	res.send("Hello World");
});

//ROUTE to create a Task
app.post("/tasks", (req, res)=>{

Task.findOne({name:req.body.name}, (err, result)=> {

	if (result !== null && result.name == req.body.name) {
		return res.send("Duplicate Task Found");
	} else {

		let newTask = new Task ({
			name: req.body.name
		});
		newTask.save((saveErr, savedTask)=> {
			if(saveErr) {
			return console.error(saveErr)
		} else {
			
				return res.status(201).send("New Task created");
			}

			

		})
	}

});


})

//Get all the tasks
app.get("/tasks", (req, res)=>{
	Task.find({},(err, result)=>{
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data:result
			})
		}
	})
})









//activity
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("User", userSchema);


app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if (result !== null && result.username == req.body.username){
			return res.send("New User registered");
		}
		else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr)
				}
				else{
					return res.status(201).send("Duplicate User registered")
				}
			})
		}
	})
})

app.post("/signup", (req, res) => {
	User.find({}, (err,result) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data:result
			})
		}
	})
})











app.listen(port, () => console.log(`
	Server is running at port: ${port}`));
